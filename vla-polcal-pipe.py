############################################################################################################
#
#  CASA pipeline for polarization calibration based on the recipe from https://casaguides.nrao.edu/
#
#  This script is meant to be used with:
#    *   Well known polarization angle calibrator (such as 3C286)
#    *   Un-polarized source as D-term (leakage) calibrator
#    *   VLA
#    *   C-band (4-8 GHz)
#    *   CASA 6.4.1
#    *   pipeline 2022.2.0.64
#
#  Not tested for other telescopes or other versions or other calibrators or other frequencies!!
#
#  Author:   Rohit Dokara (MPIfR)
#  Version:  2022 December
#
############################################################################################################


import numpy as np
myms     = '27.ms'                                      # MS to calibrate
newms    = '27_calibd.ms'                               # name for the first (non-pol) calibrated MS
preflags = '27.ms.flagtemplate.txt'                     # pre-apply flags (flag template file from NRAO SRDP)
D_calibr = 'calibrator_name'                            # unpolarized calibrator for instrumental leakage (D-terms)
P_calibr = '3C286'                                      # polarized calibrator for pol angle and cross-hand delays
reffreq  = '1.0GHz'                                     # P_calibr reference frequency  (below details obtained by fitting data in PB13/PB17)
FD_I     = 17.705166                                    # P_calibr @ ref freq: Stokes I flux density
alpha    = [-0.4507,-0.1798,0.0357]                     # P_calibr @ ref freq: spectral index
polfrac  = [0.104475,0.0027669,-0.0001129]              # P_calibr @ ref freq: polarization fraction
polangle = [33*np.pi/180, 0., 0. ]                      # P_calibr @ ref freq: polarization angle
channavg = 4                                            # channel averaging factor during target splitting


############################################################################################################
#########  First, the normal calibration, but without parallactic angle correction applied.
#########  Parallactic angle correction must be done after the polarization calibration, not right now.
############################################################################################################

__rethrow_casa_exceptions = True
context = h_init()
context.set_state( 'ProjectSummary', 'observatory', 'Karl G. Jansky Very Large Array' )
context.set_state( 'ProjectSummary', 'telescope', 'EVLA' )
context.set_state( 'ProjectStructure', 'recipe_name', 'hifv_calimage_cont' )
newms = myms[:-3]+'_calibrated.ms'
try:
    hifv_importdata( vis=myms, session=['default'] )
    hifv_hanning( pipelinemode="automatic" )
    hifv_flagdata( hm_tbuff='1.5int', fracspw=0.01, filetemplate=preflags, \
                   intents='*POINTING*,*FOCUS*,*ATMOSPHERE*,*SIDEBAND_RATIO*,*UNKNOWN*,*SYSTEM_CONFIGURATION*,*UNSPECIFIED#UNSPECIFIED*' )
    hifv_vlasetjy( pipelinemode="automatic" )
    hifv_priorcals( pipelinemode="automatic" )
    hifv_syspower( pipelinemode="automatic" )
    hifv_testBPdcals( pipelinemode="automatic" )
    hifv_checkflag( checkflagmode='bpd-vla' )
    hifv_semiFinalBPdcals( pipelinemode="automatic" )
    hifv_checkflag( checkflagmode='allcals-vla' )
    hifv_solint( pipelinemode="automatic" )
    hifv_fluxboot( pipelinemode="automatic" )
    hifv_finalcals( pipelinemode="automatic" )
    # For applycal, we don't use hifv because it has parang=True.
    # We just change it to False and run the rest as is.
    applycal( vis=myms, parang=False, applymode='calflagstrict', flagbackup=True,
              gaintable = [ myms+'.hifv_priorcals.s5_2.gc.tbl', myms+'.hifv_priorcals.s5_3.opac.tbl',
                            myms+'.hifv_priorcals.s5_4.rq.tbl', myms+'.hifv_priorcals.s5_6.ants.tbl',
                            myms+'.hifv_finalcals.s13_2.finaldelay.tbl', myms+'.hifv_finalcals.s13_4.finalBPcal.tbl',
                            myms+'.hifv_finalcals.s13_5.averagephasegain.tbl',
                            myms+'.hifv_finalcals.s13_7.finalampgaincal.tbl',
                            myms+'.hifv_finalcals.s13_8.finalphasegaincal.tbl' ],
              gainfield = ['', '', '', '', '', '', '', '', ''],
              interp = ['', '', '', '', '', 'linear,linearflag', '', '', ''],
              spwmap = [[], [], [], [], [], [], [], [], []],
              calwt = [False, False, False, False, False, False, False, False, False] )
    hifv_checkflag( checkflagmode='target-vla' )
    hifv_statwt( pipelinemode="automatic" )
    hifv_plotsummary( pipelinemode="automatic" )
    # While making the images below, "Falling back to raw data" is in the log.
    # Forcing datacolumn='corrected' here makes corrected data to be used (though the warcning persists).
    hif_makeimlist( intent='PHASE,BANDPASS', specmode='cont', datacolumn='corrected' )
    hif_makeimages( hm_masking='centralregion', overwrite_on_export=True )
finally:
    h_save()


############################################################################################################
#########  Split to new MS get model details, and set model.
#########  Also check if models are correctly set.
############################################################################################################

split( vis=myms, outputvis=newms, datacolumn='corrected' )

# setjy on polarization angle calibrator
setjy( vis=newms, field=P_calibr, spw='', selectdata=False, scalebychan=True,
       standard="manual", modimage="", listmodels=False,
       fluxdensity=[FD_I,0,0,0], spix=alpha, reffreq=reffreq,
       polindex=polfrac, polangle=polangle, rotmeas=0,
       fluxdict={}, useephemdir=False, interpolation="nearest",
       usescratch=True, ismms=False )

# For unpolarized calibrator, get model details from pipeline log file (flux bootstrap step).
# Here we'll use string matching to find reference frequency, FD, and spidx (a1,a2).
# Admittedly, this is frail and not robust at all.  But it should work.
pipefolder  = context.name                                  # name of pipeline folder
search_file = pipefolder + "/html/stage12/casapy.log"       # log file in which we will seach for the flux densities etc
search_str  = "Fitted spectrum for " + D_calibr + " with"
with open(search_file) as f:
    fdline = [line for line in f if search_str in line][0]
idx0FO   = fdline.find('fitorder=')+9
idx1FO   = fdline[idx0FO:].find(':')
idx0FD   = fdline.find('Flux density = ')+15
idx1FD   = fdline[idx0FD:].find('+')
idx0freq = fdline.find('(freq=')+6
idx1freq = fdline[idx0freq:].find(' GHz')
idx0a1   = fdline.find('a_1 (spectral index) =')+22
idx1a1   = fdline[idx0a1:].find('+')
idx0a2   = fdline.find('a_2=')+4
idx1a2   = fdline[idx0a2:].find('+')
Dcal_fo   = int(fdline[idx0FO:idx0FO+idx1FO])
Dcal_FD   = float(fdline[idx0FD:idx0FD+idx1FD])
Dcal_freq = fdline[idx0freq:idx0freq+idx1freq] + 'GHz'
Dcal_a1   = float(fdline[idx0a1:idx0a1+idx1a1])
Dcal_a2   = float(fdline[idx0a2:idx0a2+idx1a2])
if Dcal_fo>2:
    raise RuntimeError("Fit order > 2, confirm that this is okay!")

# finally setjy for unpolarized calibrator
setjy( vis=newms, field=D_calibr, spw='', selectdata=False, scalebychan=True,
       standard="manual", modimage="", listmodels=False,
       fluxdensity=[Dcal_FD,0,0,0], spix=[Dcal_a1,Dcal_a2], reffreq=Dcal_freq,
       polindex=[], polangle=[], rotmeas=0,
       fluxdict={}, useephemdir=False, interpolation="nearest",
       usescratch=True, ismms=False )

# make plots of two best antennas to confirm that all models are correctly set
flagsumm  = flagdata( newms, field=P_calibr, mode='summary' )
antnames  = list(flagsumm['antenna'].keys())
flagfracs = [ flagsumm['antenna'][key]['flagged']/flagsumm['antenna'][key]['total'] for key in antnames ]
ant1,ant2 = [ antnames[ix] for ix in np.argpartition(flagfracs,1)[0:2] ]
plotms( vis=newms, field=P_calibr, correlation='RR,LL,RL,LR',
        antenna=ant1+'&'+ant2, coloraxis='corr', iteraxis='field',
        xaxis='frequency', yaxis='amp', ydatacolumn='model',
        overwrite=True, plotfile='pol0-newmodels-amp_Pcal.png', showgui=False )
plotms( vis=newms, field=P_calibr, correlation='RR,LL,RL,LR',
        antenna=ant1+'&'+ant2, coloraxis='corr', iteraxis='field',
        xaxis='frequency', yaxis='phase', ydatacolumn='model',
        overwrite=True, plotfile='pol0-newmodels-phase_Pcal.png', showgui=False )
plotms( vis=newms, field=D_calibr, correlation='RR,LL,RL,LR',
        antenna=ant1+'&'+ant2, coloraxis='corr', iteraxis='field',
        xaxis='frequency', yaxis='amp', ydatacolumn='model',
        overwrite=True, plotfile='pol0-newmodels-amp_Dcal.png', showgui=False )
plotms( vis=newms, field=D_calibr, correlation='RR,LL,RL,LR',
        antenna=ant1+'&'+ant2, coloraxis='corr', iteraxis='field',
        xaxis='frequency', yaxis='phase', ydatacolumn='model',
        overwrite=True, plotfile='pol0-newmodels-phase_Dcal.png', showgui=False )


############################################################################################################
#########  Solve for cross-hand delays, leakage, and set polarization angle.
#########  Also plot the calibration results.
############################################################################################################

kcross_mbd = newms+".Kcross_mbd"
Dtable     = newms+'.Dterms'
Xtable     = newms+'.X'
refantenna = ant1                           # fix a reference antenna.  Hope that the least flagged antenna works.
nspws      = len(flagsumm['spw'].keys())    # number of spectral windows (for spwmap parameter)

# Get a single multi-band delay (a single delay value for all spws).
# This step is actually unnecessary since we are using an unpolarized calibrator.
gaincal( vis=newms, caltable=kcross_mbd, field=P_calibr, spw='', refant=refantenna,
         gaintype="KCROSS", solint="inf", combine="scan,spw", calmode="ap",
         gaintable=[''], gainfield=[''], interp=[''], spwmap=[[]], parang=True )

# Get instrumental leakage.
# The phases may not be linear (happens if the cross-hand delay is not good enough).
# This, however, is fine since we are not solving for Q+iU.
polcal( vis=newms, caltable=Dtable, field=D_calibr, spw='', refant=refantenna,
        poltype='Df', solint='inf,2MHz', combine='scan', gaintable=[kcross_mbd],
        gainfield=[''], spwmap=[[0]*nspws], append=False )
plotms( vis=Dtable, coloraxis='corr', iteraxis='antenna', width=1400, height=800,
        xaxis='frequency', yaxis='amp',
        gridrows=5, gridcols=6, yselfscale=True,
        overwrite=True, plotfile='pol1-Dtable-amp.png', showgui=False )
plotms( vis=Dtable, coloraxis='corr', iteraxis='antenna', width=1400, height=800,
        xaxis='frequency', yaxis='phase',
        gridrows=5, gridcols=6, yselfscale=True, plotrange=[-1,-1,-180,180],
        overwrite=True, plotfile='pol1-Dtable-phase.png', showgui=False )

# Table to fix polarization angle.
# There might be a residual slope, caused by incorrect delay.  But it should be fine.
polcal( vis=newms, caltable=Xtable, spw='', field=P_calibr, solint='inf,2MHz',
        combine='scan', poltype='Xf', refant = refantenna,
        gaintable=[kcross_mbd,Dtable], append=False,
        gainfield=['',''], spwmap=[[0]*nspws,[]] )
plotms( vis=Xtable, xaxis='frequency', yaxis='phase', coloraxis='spw',
        overwrite=True, plotfile='pol1-Xtable.png', showgui=False )


############################################################################################################
#########  Apply polarization calibration tables and split out corrected target data.
#########  Also plot the final corrected data and make pol and unpol calibrator images.
############################################################################################################

applycal( vis=newms, field='', flagbackup=True,
          gaintable=[kcross_mbd,Dtable,Xtable], gainfield=['','',''],
          interp=['', '', ''], calwt=[False,False,False],
          spw='', applymode='calflagstrict', parang=True,
          spwmap=[[0]*nspws,[],[]] )

plotms( vis=newms, field=P_calibr, avgtime='100000',
        xaxis='frequency', yaxis='amp', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-amp_Pcal.png', showgui=False )
plotms( vis=newms, field=D_calibr, avgtime='100000',
        xaxis='frequency', yaxis='amp', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-amp_Dcal.png', showgui=False )
plotms( vis=newms, field=P_calibr, avgtime='100000',
        xaxis='frequency', yaxis='phase', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-phase_Pcal.png', showgui=False, plotrange=[-1,-1,-180,180] )
plotms( vis=newms, field=D_calibr, avgtime='100000',
        xaxis='frequency', yaxis='phase', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-phase_Dcal.png', showgui=False, plotrange=[-1,-1,-180,180] )
plotms( vis=newms, field=P_calibr, avgtime='100000', avgchannel='1000000',
        xaxis='UVwave', yaxis='amp', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-UVamp_Pcal.png', showgui=False )
plotms( vis=newms, field=D_calibr, avgtime='100000', avgchannel='1000000',
        xaxis='UVwave', yaxis='amp', ydatacolumn='corrected', coloraxis='corr', iteraxis='field',
        plotfile='pol2-corrected-UVamp_Dcal.png', showgui=False )

tclean( vis=newms, field=D_calibr, datacolumn="corrected",
        imagename="im_"+D_calibr+"_init", imsize=300, cell="2.0arcsec",
        stokes="IQUV", projection="SIN", specmode="mfs", reffreq="6.0GHz",
        interpolation="linear", gridder="standard", pblimit=0.0001,
        normtype="flatnoise", deconvolver="mtmfs", scales=[0,5,15],
        nterms=2, smallscalebias=0.6, pbcor=False,
        weighting="briggs", robust=0.5,
        niter=500, cycleniter=100,
        threshold=0.0, interactive=False )
tclean( vis=newms, field=P_calibr, datacolumn="corrected",
        imagename="im_"+P_calibr+"_init", imsize=300, cell="2.0arcsec",
        stokes="IQUV", projection="SIN", specmode="mfs", reffreq="6.0GHz",
        interpolation="linear", gridder="standard", pblimit=0.0001,
        normtype="flatnoise", deconvolver="mtmfs", scales=[0,5,15],
        nterms=2, smallscalebias=0.6, pbcor=False,
        weighting="briggs", robust=0.5,
        niter=500, cycleniter=100,
        threshold=0.0, interactive=False )
immath( outfile="im_"+P_calibr+'_init_pola', mode='pola',
        imagename=["im_"+P_calibr+'_init.image.tt0'], sigma='0.0Jy/beam' )

# here split out data (intent TARGET)
import os
os.system('mkdir TARGETS')
listobsout = listobs( vis=newms, intent='*TARGET*' )
targfields = [ listobsout[key]['name'] for key in list(listobsout.keys()) if 'field_' in key ]
for targfld in targfields:
    split( vis=newms, outputvis='TARGETS/'+targfld+'.ms', width=channavg, datacolumn='corrected' )



















