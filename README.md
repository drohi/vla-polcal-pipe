# VLA pipeline for polarization calibration

### CASA pipeline for polarization calibration based on the recipe from https://casaguides.nrao.edu/

This script is meant to be used with:
-   Well known polarization angle calibrator (such as 3C286)
-   Un-polarized source as D-term (leakage) calibrator
-   VLA
-   C-band (4-8 GHz)
-   CASA 6.4.1
-   pipeline 2022.2.0.64

Not tested for other telescopes or other versions or other calibrators or other frequencies!!

Author:   Rohit Dokara (MPIfR)

Version:  2022 December

NO GUARANTEES


### Requirements

-   Linux OS
-   CASA 6.4.1
-   CASA pipeline 2022.2.0.64
-   One can use a later version too (I see no reason why the script should not work), but I haven't tested any later version
